#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jitter.LinearMath;
using Jitter.Collision;
using Jitter;
using Jitter.Dynamics;
using Jitter.Collision.Shapes;
using Jitter.Dynamics.Constraints;
using JitterDemo.Scenes;
#endregion
namespace JitterDemo
{
    class Program
    {

        private World world;

        public Program()
        {
            world = new World(new CollisionSystemSAP());

           

            BuildScene();



        }
       

        private void BuildScene()
        {
            JitterDemo b = new JitterDemo();
            Domino a = new Domino(b);
            a.Build();
        }

            /// <summary>
            /// The main entry point for the application.
            /// </summary>
            //[STAThread()]
            static void Main(string[] args)
        {
            using (JitterDemo game = new JitterDemo())
            {
                //Jitter.DynamicTree dt = new Jitter.DynamicTree();

                //JBBox jb;
                //jb.Min = JVector.Zero;
                //jb.Max = JVector.One;

                //JBBox jb2;
                //jb2.Min = JVector.Zero;
                //jb.Max = JVector.One * 2.0f;

                //dt.CreateProxy(ref jb, 1);
                //dt.CreateProxy(ref jb, 2);

                //JBBox testBox;
                //testBox.Min = JVector.Zero;
                //testBox.Max = JVector.One *20.0f;

                //dt.Query(bla, ref testBox);
                //dt.MoveProxy



                game.Run();
            }



        }

        private static bool bla(int i)
        {

            return true;
        }
    }
}
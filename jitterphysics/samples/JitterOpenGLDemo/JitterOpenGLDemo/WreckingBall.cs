﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jitter.LinearMath;
using Jitter.Collision;
using Jitter;
using Jitter.Dynamics;
using Jitter.Collision.Shapes;
using Jitter.Dynamics.Constraints.SingleBody;
using Jitter.Dynamics.Constraints;
using System.Drawing.Drawing2D;
#endregion

namespace JitterOpenGLDemo
{
    public class WreckingBall : DrawStuffOtk, IDisposable
    {
        private World world;

        public WreckingBall()
        {
            world = new World(new CollisionSystemBrute());

            dsSetSphereQuality(2);//Quality of the sphere increase

            Keyboard.KeyDown += new EventHandler<OpenTK.Input.KeyboardKeyEventArgs>(Keyboard_KeyDown);

            BuildScene();



        }
        void Keyboard_KeyDown(object sender, OpenTK.Input.KeyboardKeyEventArgs e)
        {
            if (e.Key == OpenTK.Input.Key.Space) ShootSphere();
            if (e.Key == OpenTK.Input.Key.R) BuildScene();
        }
        private void ShootSphere()
        {
            JVector pos, ang;
            dsGetViewPoint(out pos, out ang);// Get position and angle from the view point and det them to the variable the I pass

            RigidBody body = new RigidBody(new SphereShape(1.0f));
            body.Position = pos;

            JVector unit;
            unit.X = (float)Math.Cos(ang.X / 180.0f * JMath.Pi);
            unit.Y = (float)Math.Sin(ang.X / 180.0f * JMath.Pi);
            unit.Z = (float)Math.Sin(ang.Y / 180.0f * JMath.Pi);

            body.LinearVelocity = unit * 50.0f;

            world.AddBody(body);
        }


        private void BuildScene()
        {
            world.Clear();
            //Add ground
            RigidBody body = AddBox(new JVector(0, 0, -0.5f), JVector.Zero,
                new JVector(300, 300, 1));
            //world.AddBody(body);

            body.IsStatic = true;
            body.Tag = false;

            //Add tower of boxes
            for (int i = 0; i < 20; i++)
            {
                for (int e = i; e < 20; e++)
                {
                    AddBox(new JVector((e - i * 0.5f) * 1.01f, 0.0f, 0.5f + i * 1.0f),
                        JVector.Zero, JVector.One);
                }
            }

            //Add wrecking ball
            Shape sphereShape = new SphereShape(2.5f);
            RigidBody sphereBody = new RigidBody(sphereShape);
            Shape sphereShape2 = new SphereShape(1.0f);
            RigidBody sphereBody2 = new RigidBody(sphereShape2);
            sphereBody.Position = new JVector(10, -15, 20);
            sphereBody2.Position = new JVector(10, -5, 20);
            sphereBody2.IsStatic = true;

            world.AddBody(sphereBody);
            //world.AddBody(sphereBody2);

            Constraint ballConstraint = new PointPointDistance(sphereBody, sphereBody2, new JVector(10, -15, 20), new JVector(10, -5, 16));
            world.AddConstraint(ballConstraint);
        }

        //Add box in the world
        private RigidBody AddBox(JVector position, JVector velocity, JVector size)
        {
            BoxShape shape = new BoxShape(size);
            RigidBody body = new RigidBody(shape);
            world.AddBody(body);
            body.Position = position;
            body.Material.Restitution = 0.0f;
            body.LinearVelocity = velocity;
            body.IsActive = true;
            return body;
        }

        private bool initFrame = true;

        protected override void OnBeginRender(double elapsedTime)
        {
            //set the camera position in a good point
            if (initFrame)
            {
                dsSetViewpoint(new float[] { 30, -20, 8 }, new float[] { 150, 8, 0 });
                initFrame = false;
            }

            //Draw the line Constraint of the bodies involved
            foreach (Constraint constr in world.Constraints)
            {
                    dsDrawLine(constr.Body1.Position, constr.Body2.Position);
            }


            foreach (RigidBody body in world.RigidBodies)//for every rigid body in the world
            {
                //Draw shapes
                if (body.Shape is BoxShape)
                {
                    body.GetType();
                    BoxShape shape = body.Shape as BoxShape;

                    dsDrawBox(body.Position, body.Orientation, shape.Size);

                }
                else if (body.Shape is SphereShape)
                {
                    body.GetType();
                    SphereShape shape = body.Shape as SphereShape;

                    dsDrawSphere(body.Position, body.Orientation, shape.Radius);
                }
                else if (body.Shape is CapsuleShape)
                {
                    body.GetType();
                    CapsuleShape shape = body.Shape as CapsuleShape;

                    dsDrawCapsule(body.Position, body.Orientation, shape.Length, shape.Radius);
                }
                else if (body.Shape is CylinderShape)
                {
                    body.GetType();
                    CylinderShape shape = body.Shape as CylinderShape;
                    dsDrawCylinder(body.Position, body.Orientation, shape.Height, shape.Radius);
                }
                else if (body.Shape is CompoundShape)
                {
                    CompoundShape shape = body.Shape as CompoundShape;//conversion
                    BoxShape b = (BoxShape)shape.Shapes[0].Shape;
                    dsDrawBox(shape.Shapes[0].Position, shape.Shapes[0].Orientation, b.Size);
                    dsDrawBox(shape.Shapes[1].Position, shape.Shapes[1].Orientation, b.Size);
                }
            }

            world.Step(1.0f / 60.0f, true);

        }



       /* static void Main(string[] args)

        {
            using (WreckingBall p = new WreckingBall())
            {
                p.Run();
            }
        }*/
    }



}



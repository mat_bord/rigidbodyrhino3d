﻿#region Using Statements
using System;
using Jitter.LinearMath;
using Jitter.Collision;
using Jitter;
using Jitter.Dynamics;
using Jitter.Collision.Shapes;
using Jitter.Dynamics.Constraints;
using System.Runtime.InteropServices;
using System.Collections.Generic;
#endregion

namespace JitterOpenGLDemo 
{
    public class MydemoJitter: DrawStuffOtk , IDisposable
    {
        private World world;

        public MydemoJitter() 
        {
            world = new World(new CollisionSystemSAP());

            dsSetSphereQuality(2);//Quality of the sphere increase
            
            Keyboard.KeyDown += new EventHandler<OpenTK.Input.KeyboardKeyEventArgs>(Keyboard_KeyDown);

            BuildScene();
           


        }
        void Keyboard_KeyDown(object sender, OpenTK.Input.KeyboardKeyEventArgs e)
        {
            if (e.Key == OpenTK.Input.Key.Space) ShootSphere();
            if (e.Key == OpenTK.Input.Key.R) BuildScene();
        }
        private void ShootSphere()
        {
            JVector pos, ang;
            dsGetViewPoint(out pos, out ang);// Get position and angle from the view point and det them to the variable the I pass

            RigidBody body = new RigidBody(new SphereShape(1.0f));
            body.Position = pos;

            JVector unit;
            unit.X = (float)Math.Cos(ang.X / 180.0f * JMath.Pi);
            unit.Y = (float)Math.Sin(ang.X / 180.0f * JMath.Pi);
            unit.Z = (float)Math.Sin(ang.Y / 180.0f * JMath.Pi);

            body.LinearVelocity = unit * 50.0f;

            world.AddBody(body);
        }



        private void BuildScene()
        {
            world.Clear();

            //Trying to add some shapes
            Shape boxShape1 = new BoxShape(new JVector(1,1,1));
            Shape boxShape2 = new BoxShape(new JVector(3,2,1));
            Shape groundShape = new BoxShape(new JVector(200, 120,  1));
            Shape sphereShape = new SphereShape(1.0f);
            Shape capsuleShape = new CapsuleShape(1.0f, 1.0f);
            Shape cylinderShape = new CylinderShape(4,1);
            Shape ConeShape = new ConeShape(5, 3);

            RigidBody boxBody1 = new RigidBody(boxShape1);
            RigidBody boxBody2 = new RigidBody(boxShape2);
            RigidBody sphereBody=new RigidBody(sphereShape);
            RigidBody capsuleBody=new RigidBody(capsuleShape);
            RigidBody cylinderBody = new RigidBody(cylinderShape);
            RigidBody groundBody = new RigidBody(groundShape);
            RigidBody coneBody = new RigidBody(ConeShape);

            SoftBody clothBody = new SoftBody(30,30,0.5f);

            boxBody1.Position = new JVector(0, 0,12f);
            boxBody2.Position = new JVector(0, -10, 3);
            sphereBody.Position = new JVector(0, -7,3);
            capsuleBody.Position = new JVector(0, -3.0f, 3);
            cylinderBody.Position = new JVector(2, 3, 3);
            coneBody.Position = new JVector(0, 7.0f, 3);

            clothBody.Translate(new JVector(-5,20,-5));
            clothBody.Rotate(JMatrix.CreateRotationX((float)(90 * (Math.PI)) / 180) , new JVector(0,0,0));
            // make the body static, so it can't be moved
            groundBody.IsStatic = true;
            //sphereBody.IsStatic = true;

            //define the angle of rotation
            double angleDeg =180;
            // converting value to radians 
            double angleRad = (angleDeg * (Math.PI)) / 180;
            //rotate the plan
            groundBody.Orientation = new JMatrix(1,0,0,0,(float) Math.Cos(angleRad), (float)-Math.Sin(angleRad),0, (float)Math.Sin(angleRad), (float)Math.Cos(angleRad)) ;
            //boxBody1.IsStatic = true;
           // add the bodies to the world.
            world.AddBody(boxBody1);
            //world.AddBody(sphereBody);
            //world.AddBody(boxBody2);
            //world.AddBody(capsuleBody);
            //world.AddBody(cylinderBody);
            //world.AddBody(clothBody);
            world.AddBody(groundBody);
            //world.AddBody(coneBody);

            // Build the CompoundShape.TransformedShape structure,
            // containing "normal" shapes and position/orientation
            // information.
         /*   CompoundShape.TransformedShape[] transformedShapes =
                new CompoundShape.TransformedShape[2];

            //define the angle of rotation
            double angleD = 90;
            // converting value to radians 
            double angleR = (angleD * (Math.PI)) / 180;
            //rotate the plan
            JMatrix rotated = new JMatrix(1, 0, 0, 0, (float)Math.Cos(angleR), (float)-Math.Sin(angleR), 0, (float)Math.Sin(angleR), (float)Math.Cos(angleR));


            // the first "sub" shape. A rotatated boxShape.
            transformedShapes[0] = new CompoundShape.TransformedShape(
                boxShape1, rotated, new JVector(0, 0, 5));

            // the second "sub" shape.
            transformedShapes[1] = new CompoundShape.TransformedShape(
                boxShape2, JMatrix.Identity, new JVector(0, 0, 5));
            // Pass the CompoundShape.TransformedShape structure to the compound shape.
            CompoundShape compoundShape = new CompoundShape(transformedShapes);

            RigidBody compoundBody = new RigidBody(compoundShape);
            compoundBody.IsStatic = false;

            world.AddBody(compoundBody);*/

        }

        private bool initFrame = true;

        protected override void OnBeginRender(double elapsedTime)
        {
            //set the camera position in a good point
            if (initFrame)
                {
                    dsSetViewpoint(new float[] { -20,0,5 }, new float[] {0, 0, 0 });
                    initFrame = false;
                }

            //Draw the line Constraint of the bodies involved
            foreach (Constraint constr in world.Constraints)
            {
                dsDrawLine(constr.Body1.Position, constr.Body1.BoundingBox.Center);
            }
                

            foreach (RigidBody body in world.RigidBodies)//for every rigid body in the world
            {
                //Draw shapes
                if (body.Shape is BoxShape)
                {
                    body.GetType();
                    BoxShape shape = body.Shape as BoxShape;

                    dsDrawBox(body.Position, body.Orientation, shape.Size);

                }
                else if (body.Shape is SphereShape)
                {
                    body.GetType();
                    SphereShape shape = body.Shape as SphereShape;

                    dsDrawSphere(body.Position, body.Orientation, shape.Radius);
                }
                else if (body.Shape is CapsuleShape)
                {
                    body.GetType();
                    CapsuleShape shape = body.Shape as CapsuleShape;

                    dsDrawCapsule(body.Position, body.Orientation, shape.Length, shape.Radius);
                }
                else if (body.Shape is CylinderShape)
                {
                    body.GetType();
                    CylinderShape shape = body.Shape as CylinderShape;
                    dsDrawCylinder(body.Position, body.Orientation, shape.Height, shape.Radius);
                }
                else if(body.Shape is CompoundShape)
                {                     
                    CompoundShape shape = body.Shape as CompoundShape;//conversion
                    BoxShape b = (BoxShape)shape.Shapes[0].Shape;
                    BoxShape f = (BoxShape)shape.Shapes[1].Shape;
                    dsDrawBox(shape.Shapes[0].Position, shape.Shapes[0].Orientation, b.Size);
                    dsDrawBox(shape.Shapes[1].Position, shape.Shapes[1].Orientation, f.Size);
                }


            }

            /*
            //Draw non rigid bodies
            dsDrawLine(new JVector(0, 6, 5), new JVector(0, 10, 5));
            //position equivale al centro di massa del triangolo*/
            //dsDrawTriangle(new JVector(0, 5, 3), JMatrix.Identity, new JVector(0, 5, 3), new JVector(0, 8, 3), new JVector(0, 6, 6), true);
            
            world.Step(1.0f / 100.0f, true);

        }


    
         static void Main(string[] args)

         {
             using (MydemoJitter p = new MydemoJitter())
             {
                 p.Run();
             }
         }
    }



}

//Try to apply a force when I click

//If a certain key is press calls it's own method
/*   void Mouse_KeyDown(object sender, OpenTK.Input.MouseButtonEventArgs e)
   {
       if (e.Button == OpenTK.Input.MouseButton.Right) ApplyForce();
   }

   //Apply the force to the body
   private void ApplyForce()
   {
       JVector pos, ang;
       dsGetViewPoint(out pos, out ang);
       pos = new JVector( pos.X, Mouse.XDelta, Mouse.YDelta);// Get position from the pointer

       JVector unit;
       unit.X = (float)Math.Cos(ang.X / 180.0f * JMath.Pi);
       unit.Y = (float)Math.Sin(ang.X / 180.0f * JMath.Pi);
       unit.Z = (float)Math.Sin(ang.Y / 180.0f * JMath.Pi);

       foreach (RigidBody body in world.RigidBodies)//for every rigid body in the world
       {

           if (body.IsStatic)
           {

           }
           else { 
               body.ApplyImpulse(unit * 0.01f, pos);
           }

       }


   }*/








//Compound shape

/*  // Build the CompoundShape.TransformedShape structure,
            // containing "normal" shapes and position/orientation
            // information.
            CompoundShape.TransformedShape[] transformedShapes =
                new CompoundShape.TransformedShape[2];

            //define the angle of rotation
            double angleD = 90;
            // converting value to radians 
            double angleR = (angleD * (Math.PI)) / 180;
            //rotate the plan
            JMatrix rotated = new JMatrix(1, 0, 0, 0, (float)Math.Cos(angleR), (float)-Math.Sin(angleR), 0, (float)Math.Sin(angleR), (float)Math.Cos(angleR));


            // the first "sub" shape. A rotatated boxShape.
            transformedShapes[0] = new CompoundShape.TransformedShape(
                boxShape3, rotated, new JVector(0,0,5));

            // the second "sub" shape.
            transformedShapes[1] = new CompoundShape.TransformedShape(
                boxShape3, JMatrix.Identity, new JVector(0, 0, 5));
            // Pass the CompoundShape.TransformedShape structure to the compound shape.
            CompoundShape compoundShape = new CompoundShape(transformedShapes);
            
            RigidBody compoundBody = new RigidBody(compoundShape);


            world.AddBody(compoundBody);
 */

   

/*
        //MeshShape
        List<JVector> vertices = new List<JVector>();
        List<TriangleVertexIndices> Indices = new List<TriangleVertexIndices>();

        vertices.Add(new JVector(-5.0f, 0.0f, 10.0f));
        vertices.Add(new JVector(5.0f, 0.0f, 0.0f));
        vertices.Add(new JVector(-5.0f, 2.0f, 0.0f));
        vertices.Add(new JVector(10.0f, 50.0f, 0.0f));


        Indices.Add(new TriangleVertexIndices(0,1,2));
        Indices.Add(new TriangleVertexIndices(1,3,2));

        // Build an octree of it
        Octree octree = new Octree(vertices, Indices);
        octree.BuildOctree();

        // Pass it to a new instance of the triangle mesh shape
        TriangleMeshShape triangleMeshShape = new TriangleMeshShape(octree);
        // Create a body, using the triangleMeshShape
        RigidBody triangleBody = new RigidBody(triangleMeshShape);
        triangleBody.Position = new JVector(0, 3,8);

        // Add the mesh to the world.
        world.AddBody(triangleBody);
        */

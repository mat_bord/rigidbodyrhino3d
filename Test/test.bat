@echo off
echo Starting tests
set SCRIPTDIR=C:\Users\utente24\Desktop\Matteo Bordin\rigidbodyrhino3d\Test
set RHINO="C:\Program Files\Rhino 6\System\Rhino.exe"
rem goto int_test

:int_test 
%RHINO% /nosplash /nottemplate /runscript="SetMaximizedViewport Perspective _-RunPythonScript Test-JBox.py"
%RHINO% /nosplash /nottemplate /runscript="SetMaximizedViewport Perspective _-RunPythonScript Test-JSphere.py"
%RHINO% /nosplash /nottemplate /runscript="SetMaximizedViewport Perspective _-RunPythonScript Test-JCylinder.py"
%RHINO% /nosplash /nottemplate /runscript="SetMaximizedViewport Perspective _-RunPythonScript Test-JCompound.py"


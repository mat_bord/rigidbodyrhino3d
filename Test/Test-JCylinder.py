import rhinoscriptsyntax as rs
import time
import decimal
from Rhino.Geometry         import BoundingBox
from System                 import Guid
from Rhino.UI               import Panels
from Rhino                  import RhinoApp, RhinoDoc


def PrintFile(typeTest,state, data):
    text_file = open("LogTest.txt", "a")#a as append
    text_file.write(typeTest +" -- "+ state +" -- "+ str(data) +"\n")
    text_file.close()


def truncate(corners):
    for i in range(len(corners)):
        corners[i].X=round(corners[i].X,4)
        corners[i].Y=round(corners[i].Y,4)
        corners[i].Z=round(corners[i].Z,4)
    return corners

def TestPosition(corners, cornersTest,typeTest):
    equals=True
    for i in range(len(corners)):
        corners=truncate(corners)
        cornersTest=truncate(cornersTest)
        if(str(corners[i])!=str(cornersTest[i])):
            equals=False
            PrintFile(typeTest,"false->JitterData",corners[i])
            PrintFile(typeTest,"false->RhinoData",cornersTest[i])
    if(equals):
        PrintFile(typeTest,"true","")

def Translate(typeTest,translation):
    guids = rs.AllObjects(False, False, False, False)
    xform = rs.XformTranslation(translation)
    rs.TransformObjects(guids[1], xform)
    rs.TransformObjects(guids[0], xform)
    corners= rs.BoundingBox(guids[1], None, True)
    cornersTest= rs.BoundingBox(guids[0], None, True)
    TestPosition(corners, cornersTest,typeTest)    

def ui_get_panel(guid):
    Panels.OpenPanel(guid)
    panel= Panels.GetPanel(guid, RhinoDoc.ActiveDoc.RuntimeSerialNumber)
    return panel  

def ui_get_control(panel, name):
    return panel.Controls.Find(name, True)[0]

#Creation JCylinder
typeTest="Create JCylinder"
rs.Command("JCylinder -10,0,0 -10,0,5 -10,-10,5 ", True)
guids = rs.AllObjects(False, False, False, False)
corners= rs.BoundingBox(guids[0], None, True)
guid=rs.AddCylinder( "-10,0,0","-10,0,10" , 5 )
rotate90=rs.XformRotation2(90, "5,0,0", "0,0,0")
rs.TransformObjects(guid, rotate90)
cornersTest= rs.BoundingBox(guid, None, True)
TestPosition(corners, cornersTest,typeTest)
guids = rs.AllObjects(False, False, False, False)

#X translation JCylinder
typeTest="Move Cylinder to x axes"
Translate(typeTest,[3,0,0])

#Y translation JCylinder
typeTest="Move Cylinder to y axes"
Translate(typeTest,[0,3,0])

#Z translation JCylinder
typeTest="Move Cylinder to z axes"
Translate(typeTest,[0,0,3])

#MultiTranslation JCylinder
typeTest="Move to origin"
Translate(typeTest,[-3,-3,-3])

#Rotate3d-1 JSphere
typeTest="Rotate3d-1 JSphere"
center_point="0,0,0"
angle_degrees=45
rotation_axis="15,10,0"
xform1=rs.XformRotation2(angle_degrees, rotation_axis, center_point)
rs.TransformObjects(guids[1], xform1)
corners= rs.BoundingBox(guids[1], None, True)
rs.TransformObjects(guids[0], xform1)
cornersTest= rs.BoundingBox(guids[0], None, True)
TestPosition(corners, cornersTest,typeTest)

#Rotate3d-2 JSphere
typeTest="Rotate3d-2 JSphere"
center_point="-20,15,0"
angle_degrees=130
rotation_axis="-30,10,0"
xform2=rs.XformRotation2(angle_degrees, rotation_axis, center_point)
rs.TransformObjects(guids[1], xform2)
corners= rs.BoundingBox(guids[1], None, True)
rs.TransformObjects(guids[0], xform2)
cornersTest= rs.BoundingBox(guids[0], None, True)
TestPosition(corners, cornersTest,typeTest)
guids = rs.AllObjects(False, False, False, False)
for i in range(len(guids)):
    rs.DeleteObject(guids[i])


#Play 
typeTest="JCylinder 100 frames"
panel=ui_get_panel(Guid("5B59C560-D836-4A7A-B207-B690D18AFDDE"))
rs.Command("JCylinder 0,5,0 0,5,3 0,-3,3", True)
rs.Command("JBox -30,20,0 30,-15,0 30,-15,-5", True)
guids = rs.AllObjects(False, False, False, False)
rs.SelectObjects(guids[0])
rs.Command("Move 0,0,0 0,0,-5", True)
rs.Command("Rotate3D 0,0,0 0,10,0 45", True)
control=ui_get_control(panel, "IsStaticCheckBox")
control.Checked=True
rs.Command("SelNone", True)

rs.Command("RigidBody", True)
control=ui_get_control(panel, "Play")
control.PerformClick()
centerBboxj=rs.BoundingBox(guids[1])
centerj = rs.PointDivide(rs.PointAdd(centerBboxj[6], centerBboxj[0]), 2.0)
centerTest=[30.0281, 0.6573, -39.3416]
for i in range(len(centerj)):
    centerj[i]=round(centerj[i],4)
    centerTest[i]=round(centerTest[i],4)

equals=True
for i in range(len(centerj)):       
    if(str(centerj[i])!=str(centerTest[i])):
        equals=False
        PrintFile(typeTest,"false->DocData",centerj[i])
        PrintFile(typeTest,"false->TestData",centerTest[i] )
if(equals):
    PrintFile(typeTest,"true","")


#time.sleep(5)
rs.DocumentModified(False)
rs.Exit()






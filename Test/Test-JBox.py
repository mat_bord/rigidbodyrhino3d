import rhinoscriptsyntax as rs
import time
import decimal
from Rhino.Geometry         import BoundingBox
from System                 import Guid
from Rhino.UI               import Panels
from Rhino                  import RhinoApp, RhinoDoc


def PrintFile(typeTest,state, data):
    text_file = open("LogTest.txt", "a")#a as append
    text_file.write(typeTest +" -- "+ state +" -- "+ str(data) +"\n")
    text_file.close()

def ResetFile():
    text_file = open("LogTest.txt", "w")
    text_file.write("")
    text_file.close()

def truncatePoint(corners):
    for i in range(len(corners)):
        corners[i].X=round(corners[i].X,4)
        corners[i].Y=round(corners[i].Y,4)
        corners[i].Z=round(corners[i].Z,4)
    return corners

def TestPosition(corners, cornersTest,typeTest):
    equals=True
    corners=truncatePoint(corners)
    cornersTest=truncatePoint(cornersTest)
    for i in range(len(corners)):       
        if(str(corners[i])!=str(cornersTest[i])):
            equals=False
            PrintFile(typeTest,"false->JitterData",corners[i])
            PrintFile(typeTest,"false->RhinoData",cornersTest[i])
    if(equals):
        PrintFile(typeTest,"true","")

def Translate(typeTest,translation,comparisonBox):
    guids = rs.AllObjects(False, False, False, False)
    xform = rs.XformTranslation(translation)
    rs.TransformObjects(guids[0], xform)
    corners= rs.BoundingBox(guids[0], None, True)
    #create a new box to compare corners coordinates
    rs.Command(comparisonBox, True)
    guids = rs.AllObjects(False, False, False, False)
    cornersTest= rs.BoundingBox(guids[0], None, True)
    rs.DeleteObject(guids[0])
    TestPosition(corners, cornersTest,typeTest)  

def ui_get_panel(guid):
    Panels.OpenPanel(guid)
    panel= Panels.GetPanel(guid, RhinoDoc.ActiveDoc.RuntimeSerialNumber)
    return panel  

def ui_get_control(panel, name):
    return panel.Controls.Find(name, True)[0]

ResetFile()
#Creation JBox
typeTest="Create JBox"
rs.Command("JBox -10,10,0 10,-10,0 10,-10,10", True)
guids = rs.AllObjects(False, False, False, False)
corners= rs.BoundingBox(guids[0], None, True)
rs.Command("Box -10,10,0 10,-10,0 10,-10,10", True)
guids = rs.AllObjects(False, False, False, False)
cornersTest= rs.BoundingBox(guids[0], None, True)
rs.DeleteObject(guids[0])
TestPosition(corners, cornersTest,typeTest)

#X translation JBox
typeTest="Move box to x axes"
Translate(typeTest,[3,0,0],"Box -7,10,0 13,-10,0 13,-10,10")

#Y translation JBox
typeTest="Move box to y axes"
Translate(typeTest,[0,3,0],"Box -7,13,0 13,-7,0 13,-7,10")

#Z translation JBox
typeTest="Move box to z axes"
Translate(typeTest,[0,0,3],"Box -7,13,3 13,-7,3 13,-7,13")

#MultiTranslation JBox
typeTest="Move to origin"
Translate(typeTest,[-3,-3,-3],"Box -10,10,0 10,-10,0 10,-10,10")

#Rotate3d-1 JBox
typeTest="Rotate3d-1 JBox"
center_point="0,0,0"
angle_degrees=45
rotation_axis="15,10,0"
xform1=rs.XformRotation2(angle_degrees, rotation_axis, center_point)
guids = rs.AllObjects(False, False, False, False)
rs.TransformObjects(guids[0], xform1)
corners= rs.BoundingBox(guids[0], None, True)
rs.Command("Box -10,10,0 10,-10,0 10,-10,10", True)
guids = rs.AllObjects(False, False, False, False)
rs.TransformObjects(guids[0], xform1)
cornersTest= rs.BoundingBox(guids[0], None, True)
rs.DeleteObject(guids[0])
TestPosition(corners, cornersTest,typeTest)

#Rotate3d-2 JBox
typeTest="Rotate3d-2 JBox"
center_point="-20,15,0"
angle_degrees=130
rotation_axis="-30,10,0"
xform2=rs.XformRotation2(angle_degrees, rotation_axis, center_point)
guids = rs.AllObjects(False, False, False, False)
rs.TransformObjects(guids[0], xform2)
corners= rs.BoundingBox(guids[0], None, True)
rs.Command("Box -10,10,0 10,-10,0 10,-10,10", True)
guids = rs.AllObjects(False, False, False, False)
rs.TransformObjects(guids[0], xform1)
rs.TransformObjects(guids[0], xform2)
cornersTest= rs.BoundingBox(guids[0], None, True)
rs.DeleteObject(guids[0])
rs.DeleteObject(guids[1])
TestPosition(corners, cornersTest,typeTest)

#Play 
typeTest="JBox 100 frames"
panel=ui_get_panel(Guid("5B59C560-D836-4A7A-B207-B690D18AFDDE"))
rs.Command("JBox -5,5,0 5,-5,0 5,-5,5", True)
rs.Command("JBox -30,20,0 30,-15,0 30,-15,-5", True)
guids = rs.AllObjects(False, False, False, False)
rs.SelectObjects(guids[0])
rs.Command("Move 0,0,0 0,0,-5", True)
rs.Command("Rotate3D 0,0,0 0,10,0 45", True)
control=ui_get_control(panel, "IsStaticCheckBox")
control.Checked=True
rs.Command("SelNone", True)

rs.Command("RigidBody", True)
control=ui_get_control(panel, "Play")
control.PerformClick()
centerBboxj=rs.BoundingBox(guids[1])
centerj = rs.PointDivide(rs.PointAdd(centerBboxj[6], centerBboxj[0]), 2.0)
centerTest=[30.5494,-0.5778,-40.414]
for i in range(len(centerj)):
    centerj[i]=round(centerj[i],4)
    centerTest[i]=round(centerTest[i],4)

equals=True
for i in range(len(centerj)):       
    if(str(centerj[i])!=str(centerTest[i])):
        equals=False
        PrintFile(typeTest,"false->DocData",centerj[i])
        PrintFile(typeTest,"false->TestData",centerTest[i] )
if(equals):
    PrintFile(typeTest,"true","")

rs.DocumentModified(False)
rs.Exit()






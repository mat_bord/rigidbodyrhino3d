import rhinoscriptsyntax as rs
import time
import decimal
from Rhino.Geometry         import BoundingBox
from System                 import Guid
from Rhino.UI               import Panels
from Rhino                  import RhinoApp, RhinoDoc

def PrintFile(typeTest,state, data):
    text_file = open("LogTest.txt", "a")#a as append
    text_file.write(typeTest +" -- "+ state +" -- "+ str(data) +"\n")
    text_file.close()

def truncatePoint(corners):
    for i in range(len(corners)):
        corners[i]=round(corners[i],4)
    return corners

def TestPosition(corners, cornersTest,typeTest):
    equals=True
    corners=truncatePoint(corners)
    cornersTest=truncatePoint(cornersTest)
    for i in range(len(corners)):
        if(str(corners[i])!=str(cornersTest[i])):
            equals=False
            PrintFile(typeTest,"false-> JitterData",corners[i])
            PrintFile(typeTest,"false-> RhinoData",cornersTest[i])
    if(equals):
        PrintFile(typeTest,"true","")

def ui_get_panel(guid):
    Panels.OpenPanel(guid)
    panel= Panels.GetPanel(guid, RhinoDoc.ActiveDoc.RuntimeSerialNumber)
    return panel  

def ui_get_control(panel, name):
    return panel.Controls.Find(name, True)[0]

#Creation JCompound
typeTest="Create JCompound"
rs.Command("JBox -5,10,0 5,-10,0 5,-10,5", True)
rs.Command("JBox 0,0,0 15,-10,0 15,-10,15", True)
guids = rs.AllObjects(False, False, False, False)
rs.SelectObjects(guids)
rs.Command("JCompoundShape ", True)
guids = rs.AllObjects(False, False, False, False)
centerBboxj=rs.BoundingBox(guids[0])
centerj = rs.PointDivide(rs.PointAdd(centerBboxj[4], centerBboxj[0]), 2.0)

rs.Command("Box -5,10,0 5,-10,0 5,-10,5", True)
rs.Command("Box 0,0,0 15,-10,0 15,-10,15", True)
guids = rs.AllObjects(False, False, False, False)
boolguid=guids[0],guids[1]
rs.BooleanUnion(boolguid,True)
guids = rs.AllObjects(False, False, False, False)
centerBboxr=rs.BoundingBox(guids[0])
centerr = rs.PointDivide(rs.PointAdd(centerBboxr[4], centerBboxr[0]), 2.0)#findthe center of the Bbox

TestPosition(centerj, centerr,typeTest)


#Translate JCompound
typeTest="Translate JCompound"
xform = rs.XformTranslation([3,3,3])
rs.TransformObjects(guids[1], xform)
rs.TransformObjects(guids[0], xform)
centerBboxj=rs.BoundingBox(guids[1])
centerj = rs.PointDivide(rs.PointAdd(centerBboxj[4], centerBboxj[0]), 2.0)
centerBboxr=rs.BoundingBox(guids[0])
centerr = rs.PointDivide(rs.PointAdd(centerBboxr[4], centerBboxr[0]), 2.0)
TestPosition(centerj, centerr,typeTest)

#Rotate3d-1 JCompound
typeTest="Rotate3d-1 JCompound"
center_point="0,0,0"
angle_degrees=45
rotation_axis="15,10,0"
xform=rs.XformRotation2(angle_degrees, rotation_axis, center_point)
rs.TransformObjects(guids[1], xform)
rs.TransformObjects(guids[0], xform)
centerBboxj=rs.BoundingBox(guids[1])
centerj = rs.PointDivide(rs.PointAdd(centerBboxj[4], centerBboxj[0]), 2.0)
centerBboxr=rs.BoundingBox(guids[0])
centerr = rs.PointDivide(rs.PointAdd(centerBboxr[4], centerBboxr[0]), 2.0)
TestPosition(centerj, centerr,typeTest)

#Rotate3d-2 JCompound
typeTest="Rotate3d-2 JCompound"
center_point="-20,15,0"
angle_degrees=130
rotation_axis="-30,10,0"
xform=rs.XformRotation2(angle_degrees, rotation_axis, center_point)
rs.TransformObjects(guids[1], xform)
rs.TransformObjects(guids[0], xform)
centerBboxj=rs.BoundingBox(guids[1])
centerj = rs.PointDivide(rs.PointAdd(centerBboxj[4], centerBboxj[0]), 2.0)
centerBboxr=rs.BoundingBox(guids[0])
centerr = rs.PointDivide(rs.PointAdd(centerBboxr[4], centerBboxr[0]), 2.0)
TestPosition(centerj, centerr,typeTest)
guids = rs.AllObjects(False, False, False, False)
for i in range(len(guids)):
    rs.DeleteObject(guids[i])


#Play 
typeTest="JCompound 100 frames"
panel=ui_get_panel(Guid("5B59C560-D836-4A7A-B207-B690D18AFDDE"))
rs.Command("JBox -7,5,0 8,0,0 8,0,5", True)
rs.Command("JBox 5,5,0 1,0,0 1,0,-5", True)
guids = rs.AllObjects(False, False, False, False)
rs.SelectObjects(guids)
rs.Command("JCompoundShape ", True)
rs.Command("JBox -30,20,0 30,-15,0 30,-15,-5", True)
guids = rs.AllObjects(False, False, False, False)
rs.SelectObjects(guids[0])
rs.Command("Move 0,0,0 0,0,-5", True)
rs.Command("Rotate3D 0,0,0 0,10,0 45", True)
control=ui_get_control(panel, "IsStaticCheckBox")
control.Checked=True
rs.Command("SelNone", True)

rs.Command("RigidBody", True)
control=ui_get_control(panel, "Play")
control.PerformClick()
centerBboxj=rs.BoundingBox(guids[1])
centerj = rs.PointDivide(rs.PointAdd(centerBboxj[6], centerBboxj[0]), 2.0)
centerTest=[33.5088,1.1648,-38.4714]
for i in range(len(centerj)):
    centerj[i]=round(centerj[i],4)
    centerTest[i]=round(centerTest[i],4)

equals=True
for i in range(len(centerj)):       
    if(str(centerj[i])!=str(centerTest[i])):
        equals=False
        PrintFile(typeTest,"false->DocData",centerj[i])
        PrintFile(typeTest,"false->TestData",centerTest[i] )
if(equals):
    PrintFile(typeTest,"true","")



rs.DocumentModified(False)
rs.Exit()








